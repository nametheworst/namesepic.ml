FROM gitpod/workspace-full

USER gitpod

RUN sudo apt update && sudo apt upgrade -yq && python -m pip install --upgrade pip && \
    sudo apt install -yq hugo 